%global cluster jnr
%global sover 1.2
Name:                jffi
Version:             1.3.13
Release:             1
Summary:             Java Foreign Function Interface
License:             LGPL-3.0-or-later OR Apache-2.0
URL:                 http://github.com/jnr/jffi
Source0:             https://github.com/%{cluster}/%{name}/archive/%{name}-%{version}.tar.gz
Source3:             p2.inf
Patch0:              jffi-fix-dependencies-in-build-xml.patch
Patch1:              jffi-add-built-jar-to-test-classpath.patch
Patch2:              jffi-fix-compilation-flags.patch
Patch3:              jffi-1.2.12-no_javah.patch
Patch4:              jffi-fix-system-ffi.patch
BuildRequires:       ant ant-junit fdupes gcc libffi-devel make maven-local unzip mvn(junit:junit)
BuildRequires:       mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-assembly-plugin)
BuildRequires:       mvn(org.sonatype.oss:oss-parent:pom:)
BuildRequires:       libffi-devel mvn(org.codehaus.mojo:build-helper-maven-plugin)

Provides:            osgi(com.github.jnr.jffi) = %{version}

%description
An optimized Java interface to libffi.

%package native
Summary:             The %{name} JAR with native bits
%description native
This package contains %{name} JAR with native bits.

%package javadoc
Summary:             Javadoc for %{name}
BuildArch:           noarch
%description javadoc
This package contains the API documentation for %{name}.

%prep
%autosetup -n %{name}-%{name}-%{version} -p1
sed -i.cpu -e '/m\$(MODEL)/d' jni/GNUmakefile libtest/GNUmakefile
rm -rf archive/* jni/libffi/ jni/win32/ lib/CopyLibs/ lib/junit*
find ./ -name '*.jar' -exec rm -f '{}' \;
find ./ -name '*.class' -exec rm -f '{}' \;
build-jar-repository -s -p lib/ junit hamcrest/core
sed -i -e 's/haltonfailure="true"/haltonfailure="no"/' build.xml
sed -i -e 's/<javac/<javac debug="true"/' build.xml
%{mvn_package} 'com.github.jnr:jffi::native:' native
%{mvn_file} ':{*}' %{name}/@1 @1
sed -i 's/1.6/1.8/g' build.xml

%build
ant jar build-native -Duse.system.libffi=1
cp -p dist/jffi-*-Linux.jar archive/
%{mvn_build}
cp target/%{name}-%{version}-complete.jar target/%{name}-%{version}.jar

%install
%mvn_install
%fdupes -s %{buildroot}%{_javadocdir}
mkdir -p META-INF/
cp %{SOURCE3} META-INF/
jar uf %{buildroot}%{_jnidir}/%{name}/%{name}.jar META-INF/p2.inf
install -dm 755 %{buildroot}%{_libdir}/%{name}
unzip dist/jffi-*-Linux.jar
mv jni/*-Linux %{buildroot}%{_libdir}/%{name}/
pushd %{buildroot}%{_libdir}/%{name}/*
chmod +x lib%{name}-%{sover}.so
ln -s lib%{name}-%{sover}.so lib%{name}.so
popd

%check
sed -i 's|-Werror||' libtest/GNUmakefile
ant -Duse.system.libffi=1 test

%files -f .mfiles
%doc LICENSE

%files native -f .mfiles-native
%{_libdir}/%{name}
%doc LICENSE

%files javadoc -f .mfiles-javadoc
%doc LICENSE

%changelog
* Wed Apr 24 2024 yaoxin <yao_xin001@hoperun.com> - 1.3.13-1
- Upgrade to 1.3.13

* Sun Oct 08 2023 laokz <zhangkai@iscas.ac.cn> - 1.3.0-2
- Backport v1.3.11 patch to support riscv64

* Tue Sep 26 2023 Ge Wang <wang__ge@126.com> - 1.3.0-1
- update to version 1.3.0

* Mon Jun 12 2023 hutianxue <hutianxue@xfusion.com> - 1.2.13-4
- remove redundant macros %pkg_vcmp

* Thu May 18 2023 Wenlong Zhang <zhangwenlong@loongson.cn> - 1.2.13-3
- fix build error for loongarch64

* Thu May 18 2023 wulei <wu_lei@hoperun.com> - 1.2.13-2
- Add Buildrequires apache-parent package to fix pkg_vcmp macro not being recognized

* Thu Jul 30 2020 Jeffery.Gao <gaojianxing@huawei.com> - 1.2.13-1
- Package init
